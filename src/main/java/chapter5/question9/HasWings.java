package chapter5.question9;

interface HasWings {
    public abstract Object getWingSpan();
}