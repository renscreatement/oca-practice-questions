package chapter5.question20;

public abstract class Bird {

    public String name = "Bird";

    private void fly() {
    System.out.println("Bird is flying");
    }

  public static void main(String[] args) {
    Bird bird = new Pelican();
    bird.fly();
  }

}