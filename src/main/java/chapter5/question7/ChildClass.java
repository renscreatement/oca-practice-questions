package chapter5.question7;

public class ChildClass extends ParentClass {

    public void thisIsAChildMethod() {
        System.out.println("I am the child");
    }

}
