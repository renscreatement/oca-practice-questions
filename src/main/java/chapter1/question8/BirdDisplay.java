//package chapter1.question8;
//
//public class BirdDisplay {
//    public static void main(String[] name) {
//        System. out.println(name[1]);
//    }}
//
////In het boek staat BirdDisplay, hij compileert nu enkel als er .java achter gezet wordt.
////java src/main/java/chapter1/question8/BirdDisplay Sparrow Blue Jay
////java src/main/java/chapter1/question8/BirdDisplay Sparrow "Blue Jay"
////java src/main/java/chapter1/question8/BirdDisplay Blue Jay Sparrow
////java src/main/java/chapter1/question8/BirdDisplay "Blue Jay" Sparrow
////java src/main/java/chapter1/question8/BirdDisplay.class Sparrow "Blue Jay"
////java src/main/java/chapter1/question8/BirdDisplay.class "Blue Jay" Sparrow