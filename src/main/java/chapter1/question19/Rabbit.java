//package chapter1.question19;
//
//public class Rabbit {                         //line 1
//  public static void main(String[] args) {    //line 2
//    Rabbit one = new Rabbit();                //line 3
//    Rabbit two = new Rabbit();                //line 4
//    Rabbit three = one;                       //line 5
//    one = null;                               //line 6
//    Rabbit four = one;                        //line 7
//    three = null;                             //line 8
//    two = null;                               //line 9
//    two = new Rabbit();                       //line 10
//    System.gc();                              //line 11
//  }
//}
