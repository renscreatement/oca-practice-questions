//package chapter6.question13;
//
//public class Question13 {
//
//    public static Exception returnException() {
//        return new Exception();
//    }
//
//    public static void throwsError() throws Error {
//        System.out.println("error thrown");
//    }
//
//    public static void throwsException() throws Exception {
//        System.out.println("exception thrown");
//    }
//
//    public static void throwsRuntimeException() throws RuntimeException {
//        System.out.println("runtime exception thrown");
//    }
//
//    public static void main(String... args) {
//        Exception exception = returnException();
//        throwsError();
//        throwsException();
//        throwsRuntimeException();
//    }
//}
