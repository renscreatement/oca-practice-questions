package chapter6.practice.hopper;

import java.util.Random;

public class Bunny extends Hopper {

  public String name = "ChildHopper";

  public static void hopMethod() {
    System.out.println("Static hop method from child");
  }

  @Override
  public void perhapsHop() throws CanNotHopException {
    Random random = new Random();
    if (random.nextInt() % 2 == 3) {
      System.out.println("I am hopping");
    } else {
      System.out.println("I cannot hup");
      throw new CanNotHopException();
    }
  }
}
