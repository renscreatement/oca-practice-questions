package chapter6.practice.hopper;

public class CallerClassOkay {

  public static void main(String[] args) {
//    Hopper hopper = new Hopper();
//    try {
//      System.out.println(hopper.name);
//      hopper.hopMethod();
//      hopper.perhapsHop();
//    } catch (Exception e) {
//      System.out.println("An exception occurred");
//    }
//
//    Bunny bunny = new Bunny();
//    try {
//      System.out.println(bunny.name);
//      bunny.hopMethod();
//      bunny.perhapsHop();
//    } catch (Exception e) {
//      System.out.println("An exception occurred");
//    }

    Hopper hopperReferenceBunnyInstance = new Bunny();
    try {
      System.out.println(hopperReferenceBunnyInstance.name);
      hopperReferenceBunnyInstance.hopMethod();
      hopperReferenceBunnyInstance.perhapsHop();
    } catch (Exception e) {
      System.out.println("An exception occurred");
    }
  }
}
