package chapter6.practice.hopper;

import java.util.Random;

public class Hopper {

  public String name = "ParentHopper";

  public static void hopMethod() {
    System.out.println("Static hop method from parent");
  }

  public void perhapsHop() throws Exception {
    Random random = new Random();
    if (random.nextInt() % 2 == 0) {
      System.out.println("I am hopping");
    } else {
      System.out.println("I cannot hup");
      throw new CanNotHopException();
    }
  }
}
