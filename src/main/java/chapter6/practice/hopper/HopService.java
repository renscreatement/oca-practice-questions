//package chapter6.practice.hopper;
//
//public class HopService {
//
//  public static void main(String[] args) throws Exception {
//    Bunny bunny = new Bunny();
//    Hopper hopper = new Hopper();
//
//    callPerhapsHop(bunny);
//    callPerhapsHop(hopper);
//
//    callHop(bunny);
//    callHop(hopper);
//  }
//
//  private static void callHop(Hopper hopper) {
//    hopper.hop();
//  }
//
//  private static void callPerhapsHop(Hopper hopper) throws Exception {
//    hopper.perhapsHop();
//  }
//
////  Liskov substitution principle!!
//
////  Method overriding is one of the way by which java achieve Run Time Polymorphism.
////  The version of a method that is executed will be determined by the object that is used to invoke it.
////  If an object of a parent class is used to invoke the method, then the version in the parent class will be executed,
////  but if an object of the subclass is used to invoke the method, then the version in the child class will be executed.
////  In other words, it is the type of the object being referred to (not the type of the reference variable)
////  that determines which version of an overridden method will be executed.
//
//}
