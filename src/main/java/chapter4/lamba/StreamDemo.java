package chapter4.lamba;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StreamDemo {

    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(5);
        numbers.add(9);
        numbers.add(8);
        numbers.add(1);

        List<Integer> updatedNumbers = numbers.stream()
                .filter(number -> number < 8)
                .map(number -> number * 2)
                .collect(Collectors.toList());
    System.out.println(updatedNumbers);
    }
}
